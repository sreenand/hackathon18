CREATE TABLE IF NOT EXISTS `metadata`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_of_metadata` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `is_system_generated` bit(1) NOT NULL,
  `metadata_type` varchar(255) DEFAULT NULL,
  `node_author` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `node_name` varchar(255) NOT NULL,
  `tenant_id` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `meta_data_value` varchar(255) DEFAULT NULL,
  `event_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_index` (`node_id`,`node_name`,`author_of_metadata`,`tenant_id`,`metadata_type`,`meta_data_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hypermetadata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `metadata_type` varchar(255) DEFAULT NULL,
  `node_name` varchar(255) NOT NULL,
  `tenant_id` varchar(255) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;