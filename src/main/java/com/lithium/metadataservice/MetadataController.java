package com.lithium.metadataservice;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lithium.metadataservice.model.MetaData;
import com.lithium.metadataservice.repository.IMetaDataDao;
import com.lithium.metadataservice.repository.MetaDataRepository;

/**
 * @author Saiteja Tokala
 */
@RestController
public class MetadataController {

	private final MetaDataService metaDataService;

	private final MetaDataRepository metaDataRepository;
	public MetadataController(MetaDataService metaDataService, MetaDataRepository metaDataRepository){

		this.metaDataService = metaDataService;
		this.metaDataRepository = metaDataRepository;
	}



	@Autowired
	private IMetaDataDao api;

	@RequestMapping(method = RequestMethod.GET, value = "/metadata/sea")
	@ResponseBody
	public List<MetaData> findAll(@RequestParam(value = "search", required = false) String search) {
		List<SearchCriteria> params = new ArrayList<SearchCriteria>();
		if (search != null) {
			Pattern pattern = Pattern.compile("(\\w+?)(:|<|>)(\\w+?),");
			Matcher matcher = pattern.matcher(search + ",");
			while (matcher.find()) {
				params.add(new SearchCriteria(matcher.group(1),
						matcher.group(2), matcher.group(3)));
			}
		}
		return api.searchUser(params);
	}


	// Create a new MetaData
	@PostMapping("/metadata")
	public MetaData createNote(@Valid @RequestBody MetaData metaData) {

		return metaDataRepository.save(metaData);
	}




	// Get All Notes
	@PostMapping("/metadata/batch")
	public List<MetaData> getMetadataBatch(@RequestBody MetaDataPojo metaDataPojo) {
		final Integer nodeId = metaDataPojo.getNodeId();
		final String nodeName = metaDataPojo.getNodeName();
		final Integer authorOfMetadata = metaDataPojo.getAuthorOfMetadata();

		final String tenantId = metaDataPojo.getTenantId();
		final String metadataType = metaDataPojo.getMetadataType();
		if(nodeId !=null && nodeName !=null && authorOfMetadata !=null && tenantId !=null && metadataType !=null){
			return metaDataRepository.findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(nodeId, nodeName, authorOfMetadata, tenantId, metadataType);
		}
		if(nodeId !=null && nodeName !=null && authorOfMetadata !=null && tenantId !=null){
			return metaDataRepository.findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantId(nodeId, nodeName, authorOfMetadata, tenantId);
		}
		if(nodeId !=null && nodeName !=null && tenantId !=null && metadataType !=null){
			return metaDataRepository.findByNodeIdAndNodeNameAndTenantIdAndMetadataType(nodeId, nodeName, tenantId,metadataType);
		}
		if(nodeId !=null && nodeName !=null && authorOfMetadata !=null){
			return metaDataRepository.findByNodeIdAndNodeNameAndAuthorOfMetadata(nodeId, nodeName, authorOfMetadata);
		}
		if(nodeId !=null && nodeName !=null){
			return metaDataRepository.findByNodeIdAndNodeName(nodeId, nodeName);
		}
		return metaDataRepository.findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(metaDataPojo.getNodeId(),metaDataPojo.getNodeName(),metaDataPojo.getAuthorOfMetadata(),metaDataPojo.getTenantId(),metaDataPojo.getTenantId());
	}

	// Get a Single MetaData
	@GetMapping("/metadata/{id}")
	public ResponseEntity<MetaData> getNoteById(@PathVariable(value = "id") Long metadataId) {
		MetaData metaData = metaDataRepository.findOne(metadataId);
		if(metaData == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(metaData);
	}



	// Delete a MetaData
	@DeleteMapping("/metadata/{id}")
	public ResponseEntity<MetaData> deleteNote(@PathVariable(value = "id") Long metadataId) {
		MetaData metaData = metaDataRepository.findOne(metadataId);
		if(metaData == null) {
			return ResponseEntity.notFound().build();
		}

		metaDataRepository.delete(metaData);
		return ResponseEntity.ok().build();
	}

	@PostMapping("/metadata/delete")
	public ResponseEntity<MetaData> deleteMetaData(@RequestBody MetaDataPojo metaDataPojo) {
		final Integer nodeId = metaDataPojo.getNodeId();
		final String nodeName = metaDataPojo.getNodeName();
		final Integer authorOfMetadata = metaDataPojo.getAuthorOfMetadata();
		final String tenantId = metaDataPojo.getTenantId();
		final String metadataType = metaDataPojo.getMetadataType();

		if(nodeId ==null || nodeName ==null || authorOfMetadata ==null || tenantId ==null || metadataType ==null) {
			return ResponseEntity.badRequest().build();
		}

		List<MetaData> metaData = metaDataRepository.findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(nodeId, nodeName, authorOfMetadata, tenantId, metadataType);
		metaDataRepository.delete(metaData.get(0).getId());
		return ResponseEntity.ok().body(metaData.get(0));
	}


	@PostMapping("/metadata/sum")
	public ResponseEntity<Integer> getMetadataSum(@RequestBody MetaDataPojo metaDataPojo) {
		final Integer nodeId = metaDataPojo.getNodeId();
		final String nodeName = metaDataPojo.getNodeName();
		final Integer authorOfMetadata = metaDataPojo.getAuthorOfMetadata();
		final String tenantId = metaDataPojo.getTenantId();
		final String metadataType = metaDataPojo.getMetadataType();

		int sum = 0;


		if(nodeId !=null && nodeName !=null && authorOfMetadata !=null && tenantId !=null && metadataType !=null) {
			sum =  metaDataRepository.sumMetaDataValueByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(nodeId, nodeName, authorOfMetadata, tenantId, metadataType);
		}

		if(nodeId !=null && nodeName !=null && tenantId !=null && metadataType !=null) {
			sum =  metaDataRepository.sumMetaDataValueByNodeIdAndNodeNameAndTenantIdAndMetadataType(nodeId, nodeName, tenantId, metadataType);
		}

		return ResponseEntity.ok().body(new Integer(sum));
	}
	

}

