package com.lithium.metadataservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.lithium.metadataservice.model.MetaData;

/**
 * @author Saiteja Tokala
 */
@Repository
public interface MetaDataRepository extends JpaRepository<MetaData, Long> {
//
//	private Integer nodeId;
//	private String nodeName;
//	private Integer authorOfMetadata;
//	private String tenantId;
//	private String metadataType;

	List<MetaData> findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(int nodeId, String nodeName, int authorOfMetadata, String tenantId, String metadataType);

	//Integer sumMetaDataValueByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(int nodeId,String nodeName,int authorOfMetadata,String tenantId,String metadataType);
//	Integer sumMetaDataValueByNodeIdAndNodeNameAndTenantIdAndMetadataType(int nodeId,String nodeName,String tenantId,String metadataType);
	@Query(value="SELECT sum(meta_data_value) FROM MetaData where node_id = ?1 and node_name = ?2 and tenant_id =?3 and metadata_type = ?4", nativeQuery = true)
	Integer sumMetaDataValueByNodeIdAndNodeNameAndTenantIdAndMetadataType(int noteId, String nodeName, String tenantId, String metadata_type);
	@Query(value="SELECT sum(meta_data_value) FROM MetaData where node_id = ?1 and node_name = ?2 and author_of_metadata = ?3 and  tenant_id =?4 and metadata_type = ?5", nativeQuery = true)
	Integer sumMetaDataValueByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantIdAndMetadataType(int nodeId,String nodeName,int authorOfMetadata,String tenantId,String metadataType);

	List<MetaData> findByNodeIdAndNodeNameAndAuthorOfMetadata(int nodeId, String nodeName, int authorOfMetadata);

	List<MetaData> findByNodeIdAndNodeNameAndAuthorOfMetadataAndTenantId(int nodeId, String nodeName, int authorOfMetadata, String tenantId);
	List<MetaData> findByNodeIdAndNodeNameAndTenantIdAndMetadataType(int nodeId, String nodeName, String tenantId,String metaDataType);

	List<MetaData> findByNodeIdAndNodeName(int nodeId, String nodeName);

	List<MetaData> findByNodeId(int nodeId);

}
