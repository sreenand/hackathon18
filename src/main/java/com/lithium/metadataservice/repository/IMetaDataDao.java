package com.lithium.metadataservice.repository;

import java.util.List;

import com.lithium.metadataservice.SearchCriteria;
import com.lithium.metadataservice.model.MetaData;

/**
 * @author Saiteja Tokala
 */
public interface IMetaDataDao {
	List<MetaData> searchUser(List<SearchCriteria> params);

}
