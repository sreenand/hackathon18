package com.lithium.metadataservice.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.lithium.metadataservice.SearchCriteria;
import com.lithium.metadataservice.model.MetaData;

/**
 * @author Saiteja Tokala
 */
@Repository
public class MetaDataDao implements IMetaDataDao {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<MetaData> searchUser(List<SearchCriteria> params) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<MetaData> query = builder.createQuery(MetaData.class);
		Root r = query.from(MetaData.class);

		Predicate predicate = builder.conjunction();

		for (SearchCriteria param : params) {
			if (param.getOperation().equalsIgnoreCase(">")) {
				predicate = builder.and(predicate,
						builder.greaterThanOrEqualTo(r.get(param.getKey()),
								param.getValue().toString()));
			} else if (param.getOperation().equalsIgnoreCase("<")) {
				predicate = builder.and(predicate,
						builder.lessThanOrEqualTo(r.get(param.getKey()),
								param.getValue().toString()));
			} else if (param.getOperation().equalsIgnoreCase(":")) {
				if (r.get(param.getKey()).getJavaType() == String.class) {
					predicate = builder.and(predicate,
							builder.equal(r.get(param.getKey()),
									 param.getValue().toString()));
				} else {
					predicate = builder.and(predicate,
							builder.equal(r.get(param.getKey()), param.getValue()));
				}
			}
		}
		query.where(predicate);

		List<MetaData> result = entityManager.createQuery(query).getResultList();
		return result;
	}

}
